
# Goal
Project Contact Service(PCS) provides PM/SE names of projects to the business process management system.

It parses the relations on Sharepoint which records responsibility between PM/SE and the customers.
Parsing is regularly executing. Then, BPM will always get the last data.
Therefore, I no longer need to update the contacts in XML file manually.


## Scope

* Parse PM/SE data from share point.
* Store relation data between customers and PM/SE to JSON format and store in a file.
* Build web API to provide relationship information.
