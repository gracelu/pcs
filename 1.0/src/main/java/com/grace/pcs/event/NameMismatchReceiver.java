package com.grace.pcs.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.fn.Consumer;

@Service
public class NameMismatchReceiver implements Consumer<Event<MismatchContact>>
{
   Logger logger = LoggerFactory.getLogger(NameMismatchReceiver.class);

   @Autowired
   NameMismatchProcessor nameMismatchProcessor;

   @Override
   public void accept(Event<MismatchContact> mismatchNameEvent)
   {
      nameMismatchProcessor.setRecord(mismatchNameEvent.getData());
   }
}
