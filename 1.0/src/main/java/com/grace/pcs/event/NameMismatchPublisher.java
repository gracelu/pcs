package com.grace.pcs.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;

@Service
public class NameMismatchPublisher
{
   @Autowired
   EventBus eventBus;

   public void publishMismatchContact(String customer, String contact)
   {
      MismatchContact mismatchName = new MismatchContact(customer, contact);
      eventBus.notify(EvenType.MISMATCH, Event.wrap(mismatchName));
   }
}
