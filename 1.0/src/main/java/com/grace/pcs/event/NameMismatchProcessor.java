package com.grace.pcs.event;

import java.util.List;

public interface NameMismatchProcessor
{
   void setRecord(MismatchContact mismatchContact);
   List<String> getRecord();
}
