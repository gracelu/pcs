package com.grace.pcs.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MismatchContact
{
   private String customerName;
   private String mismatchName;

   public MismatchContact(String customerName, String mismatchName)
   {
      this.customerName = customerName;
      this.mismatchName = mismatchName;
   }

   public String getCustomerName()
   {
      return customerName;
   }

   public String getMismatchName()
   {
      return mismatchName;
   }

   public void setCustomerName(String customerName)
   {
      this.customerName = customerName;
   }

   public void setMismatchName(String mismatchName)
   {
      this.mismatchName = mismatchName;
   }

   @Override
   public boolean equals(Object obj)
   {
      MismatchContact mismatchContact = (MismatchContact) obj;
      if (mismatchContact.customerName.equals(this.customerName) && mismatchContact.mismatchName.equals(this.mismatchName))
         return true;
      return false;
   }

   @Override
   public int hashCode()
   {
      return customerName.hashCode() + mismatchName.hashCode();
   }
}
