package com.grace.pcs.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import reactor.Environment;
import reactor.bus.EventBus;

import static reactor.bus.selector.Selectors.$;

@Component
public class EventManager implements CommandLineRunner
{
   @Bean
   Environment env() {
      return Environment.initializeIfEmpty().assignErrorJournal();
   }

   @Bean
   EventBus createEventBus(Environment env) {
      return EventBus.create(env, Environment.THREAD_POOL);
   }

   @Autowired
   private EventBus eventBus;

   @Autowired
   NameMismatchReceiver nameMismatchReceiver;

   @Override
   public void run(String... strings) throws Exception
   {
      eventBus.on($(EvenType.MISMATCH), nameMismatchReceiver);
   }
}
