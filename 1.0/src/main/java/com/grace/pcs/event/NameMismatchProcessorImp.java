package com.grace.pcs.event;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@Component
public class NameMismatchProcessorImp implements NameMismatchProcessor
{
   private Set<MismatchContact> mismatchContacts = new CopyOnWriteArraySet<>();

   @Override
   public void setRecord(MismatchContact mismatchContact)
   {
      this.mismatchContacts.add(mismatchContact);
   }

   @Override
   public List<String> getRecord()
   {
      List<String> records = new ArrayList<>();
      mismatchContacts.stream().forEach(v -> records.add(convertRecord(v)));
      cleanRecord();
      return records;
   }

   private void cleanRecord()
   {
      mismatchContacts.clear();
   }

   private String convertRecord(MismatchContact mismatchContact)
   {
      return String.format("%s: %s", mismatchContact.getCustomerName(), mismatchContact.getMismatchName());
   }
}
