package com.grace.pcs.ldap;

import java.util.List;

public interface LDAPContactDao
{
   List<String> getAllEmails();
}
