package com.grace.pcs.ldap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Component;
import org.springframework.ldap.query.LdapQueryBuilder;

import javax.naming.directory.Attribute;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class LDAPContactDaoImp implements LDAPContactDao
{

   @Value("${ldap.sides}")
   String ldapSides;

   @Autowired
   LdapTemplate ldapTemplate;

   public List<String> getAllEmails()
   {
      List<String> allEmails=new ArrayList<>();
      String[] sides = StringUtils.split(ldapSides, ",");
      for (String side : sides)
      {
         List<String> sideEmails = ldapTemplate.search(LdapQueryBuilder.query().
                 base("OU=" + side).
                 attributes("mail").attributes("mail").where("objectclass").is("person").and("mail").like("*"), (AttributesMapper) attributes -> {
            Attribute mail = attributes.get("mail");
            if (mail == null)
               return "";
            else
               return (String) mail.get();

         });
         allEmails.addAll(sideEmails);
      }
      return allEmails;
   }
}
