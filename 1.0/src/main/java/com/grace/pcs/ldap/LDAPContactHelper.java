package com.grace.pcs.ldap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class LDAPContactHelper
{
   private LDAPContactDao ldapContactDao;

   List<String> allEmails = new ArrayList<>();
   Logger logger = LoggerFactory.getLogger(LDAPContactHelper.class);

   private LDAPContactHelper (@Autowired LDAPContactDao ldapContactDao)
   {
      this.ldapContactDao = ldapContactDao;
   }

   public boolean refreshEmails()
   {
      synchronized (allEmails)
      {
         return getAllEmails();
      }
   }
   private boolean getAllEmails()
   {
      try
      {
         allEmails.clear();
         allEmails.addAll(ldapContactDao.getAllEmails());
         allEmails = allEmails.stream().sorted().collect(Collectors.toList());

      } catch (Exception ex)
      {
         logger.error(ex.getMessage() + Arrays.toString(ex.getStackTrace()));
         return false;
      }
      logger.debug(String.format("Build email lists from LDAP successfully; Numbers of emails; %s", allEmails.size()));
      return true;
   }

   public String findEmail(String name)
   {
      synchronized (allEmails)
      {
         if (isEmailListEmpty())
            getAllEmails();
         if (validNameFormat(name))
         {
            Optional<String> result = allEmails.stream().filter(email -> (containFirstName(name, email) && containLastName(name, email))).findFirst();
            if (result.isPresent())
               return result.get();
         }
      }
      return StringUtils.EMPTY;

   }

   private Boolean isEmailListEmpty()
   {
      return allEmails.isEmpty();
   }

   private boolean validNameFormat(String name)
   {
      return StringUtils.containsWhitespace(name) || StringUtils.isNotEmpty(name);
   }

   private boolean containFirstName(String name, String email)
   {
      String emailFirstName = StringUtils.lowerCase(StringUtils.substringBefore(email, "."));
      String nameFirstName = StringUtils.lowerCase(StringUtils.substringBefore(StringUtils.stripAccents(name), " "));
      return StringUtils.contains(emailFirstName, nameFirstName);
   }

   private boolean containLastName(String name, String email)
   {
      String emailLastName = StringUtils.lowerCase(StringUtils.substringBetween(email, ".", "@"));
      String nameLastName = StringUtils.lowerCase(StringUtils.substringAfterLast(StringUtils.stripAccents(name), " "));
      return StringUtils.contains(emailLastName, nameLastName);
   }
}
