package com.grace.pcs.Util;

import com.grace.pcs.modules.EmailReceiver;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class SendMailUtil
{
   @Autowired
   private JavaMailSender javaMailSender;

   @Autowired
   private freemarker.template.Configuration freemarkerConfiguration;

   private String mailSplitter = ";";

   Logger logger  = LoggerFactory.getLogger(SendMailUtil.class);

   public void sendEmail(EmailReceiver emailReceiver, String templateName, Object module)
   {
      MimeMessage message = javaMailSender.createMimeMessage();

      try {
         MimeMessageHelper helper = new MimeMessageHelper(message, true);

         helper.setFrom(emailReceiver.sendFrom);
         helper.setTo(StringUtils.split(emailReceiver.sendTo, mailSplitter));
         helper.setSubject(emailReceiver.subject);
         helper.setCc(StringUtils.split(emailReceiver.sendCc, mailSplitter));
         String sendText = geFreeMarkerTemplateContent(templateName, module);
         helper.setText(sendText, true);

      }
      catch (MessagingException e) {
         throw new MailParseException(e);
      }

      javaMailSender.send(message);
   }


   public String geFreeMarkerTemplateContent(String template, Object model){
      StringBuffer content = new StringBuffer();
      try{
         content.append(FreeMarkerTemplateUtils.processTemplateIntoString(
            freemarkerConfiguration.getTemplate(template),model));
         return content.toString();
      }catch(Exception e){
         logger.error("Exception occurred while processing fmtemplate:"+e.getMessage());
      }
      return "";
   }
}
