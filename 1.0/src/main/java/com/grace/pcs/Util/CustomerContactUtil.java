package com.grace.pcs.Util;

import com.grace.pcs.modules.ContactFactory;
import com.grace.pcs.modules.CustomerContact;
import com.grace.pcs.persistence.ContactStorageDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Component
public class CustomerContactUtil
{

   @Autowired
   private ContactStorageDao contactStorageDao;

   @Autowired
   ContactFactory contactFactory;

   private Logger logger = LoggerFactory.getLogger(CustomerContactUtil.class);

   public static String transformCustomerName(String customer)
   {
      return StringUtils.trimAllWhitespace(customer).toLowerCase();
   }

   public List<CustomerContact> getAllContacts()
   {
      String jsonData = contactStorageDao.readData();
      return CustomerContact.deserialize(jsonData);
   }

   public CustomerContact getCustomerContact(String customer)
   {
      customer = transformCustomerName(customer);
      String jsonData = contactStorageDao.readData();
      List<CustomerContact> ccms = CustomerContact.deserialize(jsonData);
      CustomerContact res = getMatchContact(ccms, customer);
      logger.debug("Get customer;" + customer + ";Details;" + res.toString());
      return res;
   }

   public CustomerContact getTestContact()
   {
      return contactFactory.get("testContact").getContact();
   }

   private CustomerContact getMatchContact(List<CustomerContact> ccms,String customer)
   {
      Optional<CustomerContact> module;
      module = checkEqual(ccms,customer);
      if(module.isPresent())
         return module.get();
      module = checkContain(ccms, customer);
      if(module.isPresent())
         return module.get();
      triggerCustomerNotFound(customer);
      //sendEmailForCustomerNotFound(ccms,customer);
      return contactFactory.get("defaultContact").getContact();
   }

   private Optional<CustomerContact> checkEqual(List<CustomerContact> ccms, String customer)
   {
      return ccms.stream().filter(ccm -> ccm.getCustomerName().equals(customer)).findFirst();
   }

   private Optional<CustomerContact> checkContain(List<CustomerContact> ccms, String customer)
   {
      return ccms.stream().filter(ccm -> ccm.getCustomerName().contains(customer)).findFirst();
   }

   private void triggerCustomerNotFound(String customer)
   {
      logger.debug("Fail to match customer; %s",customer);
      //TODO
   }
}
