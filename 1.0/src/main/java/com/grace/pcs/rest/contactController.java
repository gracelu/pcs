package com.grace.pcs.rest;

import com.grace.pcs.Service.ContactService;
import com.grace.pcs.modules.CustomerContact;
import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


@RestController
public class contactController
{
   Logger logger = LoggerFactory.getLogger(contactController.class);
   @Autowired
   ContactService contactService;


   @Value("${profile.type}")
   private String type;

   @CrossOrigin
   @RequestMapping(value = "/", method = RequestMethod.GET)
   public String ping(HttpServletRequest request)
   {
      logger.debug("Get ping request from url;"+ request.getRequestURI());
      return "ping success. Profile type is " + type + ".";
   }

   @CrossOrigin
   @RequestMapping(value = "/contact", method = RequestMethod.GET)
   public CustomerContact getContact(@RequestParam(value = "customer", defaultValue = "") String customer)
   {
      return contactService.getContact(customer);
   }

   @CrossOrigin
   @RequestMapping(value = "/contacts", method = RequestMethod.GET)
   public List<CustomerContact> getAllContacts()
   {
      List<CustomerContact> res = contactService.getAllContacts();
      return res;
   }


   //--------------Testing purpose API---------------
//   @RequestMapping(value = "/update", method = RequestMethod.GET)
//   public String shareAuth() throws IOException
//   {
//      ContactUpdateScheduler contactUpdateScheduler = new ContactUpdateScheduler();
//      contactUpdateScheduler.scheduleUpdateContact();
//      return "done.";
//   }

   @RequestMapping(value = "/authpoc", method = RequestMethod.GET)
   public String updateContact() throws IOException
   {
      CredentialsProvider credsProvider = new BasicCredentialsProvider();

      credsProvider.setCredentials(new AuthScope(AuthScope.ANY), new NTCredentials("testuser", "Pa55w0rd!!", null, null));
      //credsProvider.setCredentials(new AuthScope(AuthScope.ANY), new NTCredentials("username", "password", "https://hostname", "domain"));
      logger.debug("credsProvider created.");
      CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
      StringBuilder sb = new StringBuilder();
      try
      {
         HttpGet httpget = new HttpGet("http://intranet.mycom-int.com/Projectmanagement/default.aspx");

         logger.debug("Executing request " + httpget.getRequestLine());
         CloseableHttpResponse response = httpclient.execute(httpget);
         try
         {
            String line;
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(content));
            logger.debug("BufferReader created.");
            while ((line = br.readLine()) != null)
            {
               sb.append(line);
            }
            br.close();
         }
         finally
         {
            response.close();
         }
      }
      finally
      {
         httpclient.close();
      }
      return sb.toString();
   }

   @RequestMapping(value = "/parse", method = RequestMethod.GET)
   public String parsePoc() throws IOException
   {
      CredentialsProvider credsProvider = new BasicCredentialsProvider();

      credsProvider.setCredentials(new AuthScope(AuthScope.ANY), new NTCredentials("testuser", "Pa55w0rd!!", null, null));
      logger.debug("credsProvider created.");
      CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
      StringBuilder sb = new StringBuilder();
      try
      {
         HttpGet httpget = new HttpGet("http://intranet.mycom-int.com/Projectmanagement/default.aspx");

         logger.debug("Executing request " + httpget.getRequestLine());
         CloseableHttpResponse response = httpclient.execute(httpget);
         try
         {
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            logger.debug("get content.");
            Document doc = Jsoup.parse(content, "UTF-8", "");
            logger.debug("create document.");
            Elements elements = doc.getElementsByClass("ms-vb2");
            for (int i = 0; i < elements.size(); i += 8)
            {
               sb.append(elements.get(i).text());
            }
         }
         catch (Exception ex)
         {
            logger.error(ex.getMessage());
         }
         finally
         {
            response.close();
         }
      }
      finally
      {
         httpclient.close();
      }
      return sb.toString();
   }

   @RequestMapping(value = "/mailtest", method = RequestMethod.GET)
   public String ldapTest()
   {
      //todo
      return "mail test done.";
   }
}
