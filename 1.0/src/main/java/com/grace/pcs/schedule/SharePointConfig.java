package com.grace.pcs.schedule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SharePointConfig
{

   @Bean
   public SharePointDao sharePointDao()
   {
      return new SharePointDaoImp();
   }
}
