package com.grace.pcs.schedule;

import java.io.IOException;
import java.util.List;

import org.jsoup.select.Elements;

public interface SharePointDao
{
   List<String> getCustomerContacts() throws IOException;
}
