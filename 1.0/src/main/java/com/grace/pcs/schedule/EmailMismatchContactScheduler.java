package com.grace.pcs.schedule;

import com.grace.pcs.Util.SendMailUtil;
import com.grace.pcs.event.NameMismatchProcessor;
import com.grace.pcs.modules.EmailReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EmailMismatchContactScheduler
{
   Logger logger = LoggerFactory.getLogger(EmailMismatchContactScheduler.class);
   @Autowired
   private SendMailUtil sendMailUtil;
   @Autowired
   NameMismatchProcessor nameMismatchProcessor;

   @Autowired
   @Qualifier("sharePointTypoReceiver")
   EmailReceiver emailReceiver;
   @Value("${mail.template}")
   String emailTemplate;
   @Value("${mail.template.key}")
   String emailTemplateKey;

   @Scheduled(cron="0 0 8 * * MON-FRI")
   public void notifyMismatchContact()
   {
      List<String> records = nameMismatchProcessor.getRecord();
      if (records.isEmpty())
      {
         logger.debug("No mismatch contacts to send out.");
         return;
      }
      logger.debug("Send mismatch contacts.");
      Map<String, Object> model = new HashMap<>();
      model.put(emailTemplateKey, records);
      sendMailUtil.sendEmail(emailReceiver, emailTemplate, model);
   }
}
