package com.grace.pcs.schedule;

import com.grace.pcs.modules.CustomerContact;
import com.grace.pcs.modules.CustomerContactParser;
import com.grace.pcs.persistence.ContactStorageDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class ContactUpdateScheduler
{
   Logger logger = LoggerFactory.getLogger(ContactUpdateScheduler.class);

   @Autowired
   SharePointDao sharePointDao;
   @Autowired
   CustomerContactParser customerContactParser;
   @Autowired
   ContactStorageDao contactStorageDao;

   @Scheduled(fixedDelay = 1800000)
   public void scheduleUpdateContact() throws IOException
   {
      List<String> contacts = sharePointDao.getCustomerContacts();
      if (!contacts.isEmpty())
      {
         List<CustomerContact> customerContacts = customerContactParser.resolver(contacts);
         contactStorageDao.storeData(CustomerContact.serialize(customerContacts));
      }
   }

}
