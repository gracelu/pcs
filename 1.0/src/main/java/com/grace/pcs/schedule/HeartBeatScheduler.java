package com.grace.pcs.schedule;

import com.grace.pcs.Util.SendMailUtil;
import com.grace.pcs.modules.EmailReceiver;
import com.grace.pcs.persistence.ContactStorageDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class HeartBeatScheduler
{

   @Autowired
   private ContactStorageDao contactStorageDao;

   @Autowired
   @Qualifier("heartBeatReceiver")
   private EmailReceiver emailReceiver;

   @Autowired
   private SendMailUtil sendMailUtil;

   @Value("${profile.type}")
   private String profile;

   private Logger logger = LoggerFactory.getLogger(HeartBeatScheduler.class);

//   @Scheduled(cron="0 0 8 * * *")
//   @Scheduled(fixedDelay = 1800000)
   public void sendDailyMail()
   {
      String heartBeatTemplateName = "heartBeatTemplate.ftl";
      String nowDate = new SimpleDateFormat("yyyy.MM.dd 'at' hh:mm:ss").format(new Date());
      Map<String,String> model = new HashMap<>();
      model.put("profile",profile);
      model.put("today", nowDate);
      model.put("contacts", contactStorageDao.readData());
      sendMailUtil.sendEmail(emailReceiver,heartBeatTemplateName,model);
      logger.debug(String.format("Send heart beat email at %s.",nowDate));
   }
}
