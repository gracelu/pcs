package com.grace.pcs.schedule;


import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class SharePointDaoImp implements SharePointDao
{
   @Value("${sharedoc.url}")
   private String shareDocUrl;
   @Value("${sharedoc.user}")
   private String username;
   @Value("${sharedoc.password}")
   private String password;

   Logger logger = LoggerFactory.getLogger(SharePointDaoImp.class);

   public List<String> getCustomerContacts() throws IOException
   {
      Elements elements = getHtmlFromSharePoint();
      if (elements != null && elements.size() != 0)
      {
         logger.debug("Number of ms-vb2 elements is;" + elements.size());
         logger.debug("Get contacts from sharePoint is done.");
         return convertElements(elements);
      }
      else
      {
         logger.debug("Fail to get Elements from SharePoint;");
         return Collections.emptyList();
      }
   }

   private List<String> convertElements(Elements elements)
   {
      List<String> contacts = new ArrayList<>();
      for (int i = 0; i < elements.size(); i++)
         contacts.add(elements.get(i).text());
      return contacts;
   }

   private Elements getHtmlFromSharePoint() throws IOException
   {
      logger.debug("SharePoint Url: " + shareDocUrl);
      logger.debug("SharePoint user: " + username);
      logger.debug("SharePoint password: " + password);

      CredentialsProvider credsProvider = new BasicCredentialsProvider();
      credsProvider.setCredentials(new AuthScope(AuthScope.ANY), new NTCredentials(username, password, null, null));
      logger.debug("credsProvider created.");
      CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

      HttpGet httpget = new HttpGet(shareDocUrl);
      logger.debug("Executing request " + httpget.getRequestLine());
      CloseableHttpResponse response = httpclient.execute(httpget);
      InputStream content = response.getEntity().getContent();

      Document doc = Jsoup.parse(content,"UTF-8","");
      Elements elements = doc.getElementsByClass("ms-vb2");
      logger.debug("Get SharePoint Doc success.");
      return elements;
   }

}

;
