package com.grace.pcs.schedule;

import com.grace.pcs.ldap.LDAPContactHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class EmailListUpdateScheduler
{
   @Autowired
   private LDAPContactHelper ldapContactHelper;

   @Scheduled(cron="0 0 */1 * * *")
   public void updateEmailListFromLDAP()
   {
      ldapContactHelper.refreshEmails();
   }

}
