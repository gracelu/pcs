package com.grace.pcs.Service;

import com.grace.pcs.Util.CustomerContactUtil;
import com.grace.pcs.Util.SendMailUtil;
import com.grace.pcs.modules.CustomerContact;
import com.grace.pcs.modules.EmailReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Profile("production")
public class ContactServiceImp implements ContactService
{
   @Autowired
   @Qualifier("customerNotFoundReceiver")
   private EmailReceiver emailReceiver;

   @Autowired
   SendMailUtil sendMailUtil;
   @Autowired
   private CustomerContactUtil customerContactUtil;

   Logger logger = LoggerFactory.getLogger(ContactServiceImp.class);

   public CustomerContact getContact(String customer)
   {
      return customerContactUtil.getCustomerContact(customer);
   }

   public List<CustomerContact> getAllContacts()
   {
      return customerContactUtil.getAllContacts();
   }

   private void sendEmailForCustomerNotFound(List<CustomerContact> ccms, String customer)
   {
      logger.error("Could not find customer name on SharePoint;" + customer + ";Use default contacts;");
      Map<String,Object> model = new HashMap<>();
      model.put("customers", ccms);
      model.put("notFound", customer.toUpperCase());
      sendMailUtil.sendEmail(emailReceiver,"noCustomerTemplate.ftl",model);
   }
}

