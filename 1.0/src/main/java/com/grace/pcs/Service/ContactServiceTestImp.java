package com.grace.pcs.Service;

import com.grace.pcs.Util.CustomerContactUtil;
import com.grace.pcs.modules.CustomerContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("test")
public class ContactServiceTestImp implements ContactService
{

   @Autowired
   private CustomerContactUtil customerContactUtil;

   public CustomerContact getContact(String customerName)
   {
      customerContactUtil.getCustomerContact(customerName);
      return customerContactUtil.getTestContact();
   }

   public List<CustomerContact> getAllContacts()
   {
      return customerContactUtil.getAllContacts();
   }
}
