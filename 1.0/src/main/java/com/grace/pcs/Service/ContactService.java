package com.grace.pcs.Service;

import com.grace.pcs.modules.CustomerContact;

import java.util.List;


public interface ContactService
{
   CustomerContact getContact(String customerName);
   List<CustomerContact> getAllContacts();
}
