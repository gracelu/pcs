package com.grace.pcs.modules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


public class EmailReceiver
{
   public String sendTo;
   public String sendCc;
   public String subject;
   public String sendFrom;
}
