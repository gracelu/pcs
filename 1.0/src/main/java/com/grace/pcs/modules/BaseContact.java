package com.grace.pcs.modules;

public class BaseContact implements Contact
{
   protected CustomerContact customerContact;

   public CustomerContact getContact()
   {
      return customerContact;
   }
}
