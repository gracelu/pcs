package com.grace.pcs.modules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class EmailReceiverBeans
{

   @Value("${send.to}")
   public String sendTo;
   @Value("${send.cc}")
   public String sendCc;
   @Value("${send.from}")
   public String sendFrom;

   @Value("${pcis.admins}")
   private String pcisAdminEmails;

   /**
    *Create email receivers if system found typo on sharePoint.
    */
   @Bean
   public EmailReceiver sharePointTypoReceiver()
   {
      EmailReceiver emailReceiver = new EmailReceiver();
      emailReceiver.sendCc = sendCc;
      emailReceiver.sendFrom = sendFrom;
      emailReceiver.subject = "PM/SE names on SharePoint have spelling error";
      emailReceiver.sendTo=sendTo;
      return emailReceiver;
   }

   /**
    *Create email receivers for sending PCIS status report.
    */
   @Bean
   public EmailReceiver heartBeatReceiver()
   {
      EmailReceiver emailReceiver = new EmailReceiver();
      emailReceiver.sendCc = "";
      emailReceiver.sendFrom = sendFrom;
      emailReceiver.subject = "Status report of PCIS.";
      emailReceiver.sendTo=pcisAdminEmails;
      return emailReceiver;
   }

   /**
    *Create email receivers when customer wasn't found.
    */
   @Bean
   public EmailReceiver customerNotFoundReceiver()
   {
      EmailReceiver emailReceiver = new EmailReceiver();
      emailReceiver.sendCc = sendCc;
      emailReceiver.sendFrom = sendFrom;
      emailReceiver.subject = "PCIS can't fine the customer";
      emailReceiver.sendTo=sendTo;
      return emailReceiver;
   }
}
