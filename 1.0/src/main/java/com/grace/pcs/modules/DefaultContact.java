package com.grace.pcs.modules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("defaultContact")
public class DefaultContact extends BaseContact
{

   public DefaultContact(@Value("${contact.pm.default}") String pmEmail, @Value("${contact.se.default}") String seEmail)
   {
      customerContact = new CustomerContact(pmEmail,seEmail);
   }
}
