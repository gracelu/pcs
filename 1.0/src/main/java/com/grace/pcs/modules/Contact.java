package com.grace.pcs.modules;

public interface Contact
{
   CustomerContact getContact();
}
