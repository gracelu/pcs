package com.grace.pcs.modules;

public interface ContactFactory
{
   Contact get(String contactType);
}
