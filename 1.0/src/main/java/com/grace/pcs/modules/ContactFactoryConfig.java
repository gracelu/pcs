package com.grace.pcs.modules;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContactFactoryConfig
{
   @Bean
   public FactoryBean serviceLocatorFactoryBean() {
      ServiceLocatorFactoryBean factoryBean = new ServiceLocatorFactoryBean();
      factoryBean.setServiceLocatorInterface(ContactFactory.class);
      return factoryBean;
   }

}
