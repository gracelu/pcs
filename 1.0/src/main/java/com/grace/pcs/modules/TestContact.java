package com.grace.pcs.modules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("testContact")
public class TestContact extends BaseContact
{
   public TestContact(@Value("${contact.test.pm}") String pmEmail, @Value("${contact.test.leadse}") String seEmail)
   {
      customerContact = new CustomerContact(pmEmail,seEmail);
   }

}
