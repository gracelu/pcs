package com.grace.pcs.modules;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerContact
{
   private String customerName;
   private String accountManager;
   private String accountManagerEmail;
   private String projectManager;
   private String projectManagerEmail;
   private String leadSe;
   private String leadSeEmail;
   private String deliveryManager;
   private String deliveryManagerEmail;
   private String supportPrime;
   private String supportPrimeEmail;
   private String country;
   private String timezone;

   private static Logger logger = LoggerFactory.getLogger(CustomerContact.class);

   public static String serialize(List<CustomerContact> contacts)
   {
      ObjectMapper mapper = new ObjectMapper();
      String res = "";
      try
      {
         res = mapper.writeValueAsString(contacts);
         return res;
      }catch (Exception ex)
      {
         logger.error(ex.getMessage());
      }
      return res;
   }

   public static List<CustomerContact> deserialize(String json)
   {
      List<CustomerContact> res = new ArrayList<>();
      try
      {
         ObjectMapper mapper = new ObjectMapper();
         res = Arrays.asList(mapper.readValue(json, CustomerContact[].class));
      }
      catch (Exception ex)
      {
         logger.error(ex.getMessage());
      }
      return res;
   }

   public CustomerContact()
   {
      customerName = StringUtils.EMPTY;
      accountManager = StringUtils.EMPTY;
      accountManagerEmail = StringUtils.EMPTY;
      projectManager = StringUtils.EMPTY;
      projectManagerEmail = StringUtils.EMPTY;
      leadSe = StringUtils.EMPTY;
      leadSeEmail = StringUtils.EMPTY;
      deliveryManager = StringUtils.EMPTY;
      deliveryManagerEmail = StringUtils.EMPTY;
      supportPrime = StringUtils.EMPTY;
      supportPrimeEmail = StringUtils.EMPTY;
      country = StringUtils.EMPTY;
      timezone = StringUtils.EMPTY;
   }

   public CustomerContact(String pmEmail, String leadSeEmail)
   {
      this();
      this.projectManagerEmail = pmEmail;
      this.leadSeEmail = leadSeEmail;
   }

   public String getCustomerName()
   {
      return customerName;
   }

   public void setCustomerName(String customerName)
   {
      this.customerName = customerName;
   }

   public String getAccountManager()
   {
      return accountManager;
   }

   public void setAccountManager(String accountManager)
   {
      this.accountManager = accountManager;
   }

   public String getAccountManagerEmail()
   {
      return accountManagerEmail;
   }

   public void setAccountManagerEmail(String accountManagerEmail)
   {
      this.accountManagerEmail = accountManagerEmail;
   }

   public String getProjectManager()
   {
      return projectManager;
   }

   public void setProjectManager(String projectManager)
   {
      this.projectManager = projectManager;
   }

   public String getProjectManagerEmail()
   {
      return projectManagerEmail;
   }

   public void setProjectManagerEmail(String projectManagerEmail)
   {
      this.projectManagerEmail = projectManagerEmail;
   }

   public String getLeadSe()
   {
      return leadSe;
   }

   public void setLeadSe(String leadSe)
   {
      this.leadSe = leadSe;
   }

   public String getLeadSeEmail()
   {
      return leadSeEmail;
   }

   public void setLeadSeEmail(String leadSeEmail)
   {
      this.leadSeEmail = leadSeEmail;
   }

   public String getDeliveryManager()
   {
      return deliveryManager;
   }

   public void setDeliveryManager(String deliveryManager)
   {
      this.deliveryManager = deliveryManager;
   }

   public String getDeliveryManagerEmail()
   {
      return deliveryManagerEmail;
   }

   public void setDeliveryManagerEmail(String deliveryManagerEmail)
   {
      this.deliveryManagerEmail = deliveryManagerEmail;
   }

   public String getSupportPrime()
   {
      return supportPrime;
   }

   public void setSupportPrime(String supportPrime)
   {
      this.supportPrime = supportPrime;
   }

   public String getSupportPrimeEmail()
   {
      return supportPrimeEmail;
   }

   public void setSupportPrimeEmail(String supportPrimeEmail)
   {
      this.supportPrimeEmail = supportPrimeEmail;
   }

   public String getCountry()
   {
      return country;
   }

   public void setCountry(String country)
   {
      this.country = country;
   }

   public String getTimezone()
   {
      return timezone;
   }

   public void setTimezone(String timezone)
   {
      this.timezone = timezone;
   }

   @Override
   public String toString()
   {
      return "customerName;"+customerName+";accountManager;"+accountManager+";projectManager;"+projectManager+";leadSe;"+leadSe+";deliveryManager;"+deliveryManager
         +";supportPrime;"+supportPrime;
   }

}
