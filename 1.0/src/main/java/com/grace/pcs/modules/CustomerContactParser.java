package com.grace.pcs.modules;

import com.grace.pcs.Util.CustomerContactUtil;
import com.grace.pcs.Util.SendMailUtil;
import com.grace.pcs.event.NameMismatchPublisher;
import com.grace.pcs.ldap.LDAPContactHelper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Component("contactHandler")
public class CustomerContactParser
{
   @Autowired
   private LDAPContactHelper ldapContactHelper;

   @Autowired
   NameMismatchPublisher nameMismatchPublisher;

   Logger logger = LoggerFactory.getLogger(CustomerContactParser.class);

   public List<CustomerContact> resolver(List<String> elements)
   {
      List<CustomerContact> customerContacts = new ArrayList<>();
      parseContactData(customerContacts, elements);
      fillEmailData(customerContacts);
      return customerContacts;
   }


   /**
    * index + 1 : account manager
    * index + 2 : project manager
    * index + 3 : leadSe
    * index + 4 : delivery manager
    * index + 5 : support prime
    * index + 6 : country
    * index + 7 : timezone.
    * @param customerContacts
    * @param elements
    */
   private void parseContactData(List<CustomerContact> customerContacts, List<String> elements)
   {
      for (int i = 0; i < elements.size(); i += 8)
      {
         CustomerContact customerContact = new CustomerContact();
         customerContact.setCustomerName(CustomerContactUtil.transformCustomerName(elements.get(i)));
         customerContact.setAccountManager(elements.get(i + 1));
         customerContact.setProjectManager(elements.get(i + 2));
         customerContact.setLeadSe(elements.get(i + 3));
         customerContact.setDeliveryManager(elements.get(i + 4));
         customerContact.setSupportPrime(elements.get(i + 5));
         customerContact.setCountry(elements.get(i + 6));
         customerContact.setTimezone(elements.get(i + 7));
         customerContacts.add(customerContact);
      }
   }

   private void fillEmailData(List<CustomerContact> customerContacts)
   {
      for(CustomerContact ccm: customerContacts)
      {
         ccm.setAccountManagerEmail(doFindEmailByName(ccm.getCustomerName(), ccm.getAccountManager()));
         ccm.setProjectManagerEmail(doFindEmailByName(ccm.getCustomerName(), ccm.getProjectManager()));
         ccm.setLeadSeEmail(doFindEmailByName(ccm.getCustomerName(), ccm.getLeadSe()));
         ccm.setDeliveryManagerEmail(doFindEmailByName(ccm.getCustomerName(), ccm.getDeliveryManager()));
         ccm.setSupportPrimeEmail(doFindEmailByName(ccm.getCustomerName(), ccm.getSupportPrime()));
      }
   }

   private String doFindEmailByName(String customer,String name)
   {
      if(invalidName(name))
         return StringUtils.EMPTY;
      String email = ldapContactHelper.findEmail(name);
      if(StringUtils.isEmpty(email))
      {
         nameMismatchPublisher.publishMismatchContact(customer, name);
      }
      return email;
   }

   private boolean invalidName(String name)
   {
      return !StringUtils.contains(name,StringUtils.SPACE);
   }
}
