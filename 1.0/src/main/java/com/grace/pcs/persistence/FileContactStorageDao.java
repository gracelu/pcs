package com.grace.pcs.persistence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.io.LineNumberReader;

import org.apache.tomcat.jni.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;


@Component
public class FileContactStorageDao implements ContactStorageDao
{
   @Value("${contact.filename}")
   private String contactFilePath;

   Logger logger = LoggerFactory.getLogger(FileContactStorageDao.class);

   public boolean storeData(String contacts)
   {
      BufferedWriter bufferedWriter = null;
      synchronized (this)
      {
         try
         {
            bufferedWriter = new BufferedWriter(new FileWriter(System.getProperty("user.dir") +
               java.io.File.separator + contactFilePath));
            bufferedWriter.write(contacts);
            logger.debug("write contact successfully.");
            return true;
         } catch (IOException e)
         {
            logger.error(e.getMessage());
            return false;
         } catch (Exception e)
         {
            logger.error(e.getMessage());
            return false;
         } finally
         {
            try
            {
               bufferedWriter.close();
            } catch (Exception e)
            {
               logger.error("Fail to close bufferedWriter;" + e.getMessage());
            }
         }
      }
   }

   public String readData()
   {
      LineNumberReader lineNumberReader = null;
      StringBuilder stringBuilder = new StringBuilder();
      synchronized (this)
      {
         try
         {
            Resource resource = new ClassPathResource(contactFilePath);
            lineNumberReader = new LineNumberReader(new BufferedReader(new FileReader(resource.getFile())));

            String line;
            while (null != (line = lineNumberReader.readLine()))
            {
               stringBuilder.append(line);
            }
         } catch (IOException e)
         {
            logger.error(e.getMessage());
         } catch (Exception e)
         {
            logger.error(e.getMessage());
         } finally
         {
            try
            {
               lineNumberReader.close();
            } catch (Exception e)
            {
               logger.error("Fail to close LineNumberReader;" + e.getMessage());
            }
         }
         return stringBuilder.toString();
      }
   }
}
