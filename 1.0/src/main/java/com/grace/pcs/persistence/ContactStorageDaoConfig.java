package com.grace.pcs.persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ContactStorageDaoConfig
{
   @Bean()
   public FileContactStorageDao fileContactStorageDao()
   {
      return new FileContactStorageDao();
   }
}
