package com.grace.pcs.persistence;

import java.util.List;
import java.util.Map;


public interface ContactStorageDao
{
   boolean storeData(String contacts);
   String readData();
}
