<html>
<body>
<div>
    Dear owner,<br>
</div>
<div>
    PCIS system can't find the customer name <b>${notFound}</b> on the following web page: "<br>
    <a href="http://intranet.mycom-int.com/Projectmanagement/default.aspx">http://intranet.mycom-int.com/Projectmanagement/default.aspx</a><br><br>
    This cause Bonita failed to send the emails to corresponding PM/SE.<br>
    Please provide the correct customer information.<br><br>

    <b>Here are the current customer's names on SharePoint:</b><br>
<#list customers as thing>
${thing.customerName}<br>
</#list>
</div>
<br>
Best regards,<br>
Dev Team
</body>
</html>
