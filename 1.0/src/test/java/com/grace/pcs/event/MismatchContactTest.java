package com.grace.pcs.event;

import junit.framework.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class MismatchContactTest
{

   @Test
   public void testContactEqual()
   {
      MismatchContact mismatchContact2 = new MismatchContact("customer", "name");
      MismatchContact mismatchContact1 = new MismatchContact("customer", "name");
      Assert.assertEquals(mismatchContact1, mismatchContact2);
   }

   @Test
   public void testContactSet()
   {
      MismatchContact mismatchContact2 = new MismatchContact("customer", "name");
      MismatchContact mismatchContact1 = new MismatchContact("customer", "name");
      Set<MismatchContact> contacts = new HashSet<>();
      contacts.add(mismatchContact1);
      contacts.add(mismatchContact2);
      Assert.assertEquals(1, contacts.size());
   }
}
